# frozen_string_literal: true

require 'rails_helper'

RSpec.describe 'Quest', type: :request, tdd: true do
  describe 'POST /quests' do
    let(:user) { create(:user) }
    let(:quest_params) do
      {
        quest: {
          name: 'Epic journey',
          description: 'Quest of a lifetime',
          user_id: user.id
        }
      }
    end

    it 'use service object' do
      let(:quest) { create(:quest) }
      expect(Quest::Create).to receive(:call).once.and_return(quest)
      post '/quests', params: quest_params
    end

    it 'creates quest record' do
      expect do
        post '/quests', params: quest_params
      end.to change { Quest.count }.by(1)
    end

    it 'returns "created" status' do
      post '/quests', params: quest_params

      expect(response.status).to eq(201)
    end

    it 'returns informative message' do
      post '/quests', params: quest_params

      expect(json).to eq('Quest was successfully created')
    end

    context 'too long name' do
      let(:long_name) do
        "If I hear any more nonsense from either of you I'll"\
        " give the order that'll destroy all spice production"\
        ' on Arrakis… forever.'
      end

      let(:quest_params) do
        {
          quest: {
            name: long_name
          }
        }
      end

      let(:error_details) do
        ['Name is too long (maximum is 50 characters)']
      end

      let(:unprocessable_entity) do
        422
      end

      it 'cannot process request, returns error' do
        post '/quests', params: quest_params

        expect(json).to eq(error_details)
        expect(response.status).to eq(unprocessable_entity)
      end
    end
  end

  describe 'GET /quests/:quest_id' do
    before(:each) do
      @quest = create(:quest)
    end

    let(:ok) do
      200
    end

    it 'returns json representation of the quest' do
      get "/quests/#{@quest.id}"

      expect(response.status).to eq(ok)
      expect(json.dig('data', 'attributes', 'questName'))
        .to eq(@quest.name)
      expect(json.dig('data', 'attributes', 'questDescription'))
        .to eq(@quest.description)
    end

    let(:not_found) do
      404
    end

    it 'handles id that is out of range' do
      get '/quests/1000000'

      expect(response.status).to eq(not_found)
    end
  end

  describe 'GET /quests' do
    let(:number_of_quests) { @number_of_quests }

    before(:all) do
      @number_of_quests = 2
      @quests = create_list(:quest, @number_of_quests)
    end

    let(:first_quest) { @quests.first }
    let(:description_path) { %w[attributes questDescription] }
    let(:name_path) { %w[attributes questName] }

    it 'returns more than one quest' do
      get '/quests'

      expect(response.status).to eq(200)
      expect(json['data']&.count).to eq(number_of_quests)
    end

    it 'returns properly serialized quests' do
      get '/quests'

      quest_to_compare = json['data'].find do |quest|
        quest['id'].to_i == first_quest.id
      end

      expect(quest_to_compare.dig(*description_path))
        .to eq(first_quest.description)
      expect(quest_to_compare.dig(*name_path))
        .to eq(first_quest.name)
    end
  end

  describe 'PUT /quests/:quest_id' do
    let(:name) { 'Original name' }
    let(:description) { 'Original description' }

    let(:updated_name) { 'Updated name' }
    let(:updated_description) { 'Updated description' }

    let(:updated_quest_params) do
      {
        quest: {
          name: updated_name,
          description: updated_description
        }
      }
    end

    before(:each) do
      @quest = create(
        :quest,
        name: name,
        description: description
      )
    end


    it 'use service object' do
      expect(Quest::Update).to receive(:call).once.and_return(@quest)
      put "/quests/#{@quest.id}", params: updated_quest_params
    end

    it 'updates given properties' do
      expect {
        put "/quests/#{@quest.id}", params: updated_quest_params
        @quest.reload
      }.to change { @quest.name }
        .to(updated_name)
        .and change { @quest.description }
        .to(updated_description)
    end

    let(:ok) { 200 }

    it 'returns appropriate status' do
      put "/quests/#{@quest.id}", params: updated_quest_params

      expect(response.status).to eq(ok)
    end

    context 'name to long' do
      let(:updated_name) do
        "If I hear any more nonsense from either of you I'll"\
        " give the order that'll destroy all spice production"\
        ' on Arrakis… forever.'
      end

      it 'does not update record' do
        expect {
          put "/quests/#{@quest.id}", params: updated_quest_params
          @quest.reload
        }.not_to(change { @quest.name })

        expect(response.status).to eq(422)
      end
    end

    context 'policy' do
      context 'authorized' do
        before { allow(QuestPolicy).to receive(:able_to_moderate?).once.and_return(true) }

        it 'allow user moderate own quests' do
          put "/quests/#{@quest.id}", params: updated_quest_params
          expect(response).to have_http_status(:ok)
        end
      end

      context 'unauthorized' do
        before { allow(QuestPolicy).to receive(:able_to_moderate?).once.and_return(false) }

        it 'do not allow user moderate not own quests' do
          put "/quests/#{@quest.id}", params: updated_quest_params
          expect(response).to have_http_status(:forbidden)
        end
      end
    end
  end

  describe 'DELETE /quests/:quest_id' do
    let(:no_content) { 204 }

    before(:each) do
      @quest = create(:quest)
    end

    it 'use service object' do
      expect(Quest::Destroy).to receive(:call).once
      delete "/quests/#{@quest.id}"
    end

    it 'deletes given quest' do
      delete "/quests/#{@quest.id}"

      expect { @quest.reload }
        .to raise_error(ActiveRecord::RecordNotFound)

      expect(response.status).to eq(no_content)
    end

    context 'policy' do
      context 'authorized' do
        before { allow(QuestPolicy).to receive(:able_to_moderate?).once.and_return(true) }

        it 'allow user moderate own quests' do
          delete "/quests/#{@quest.id}"
          expect(response).to have_http_status(:no_content)
        end
      end

      context 'unauthorized' do
        before { allow(QuestPolicy).to receive(:able_to_moderate?).once.and_return(false) }

        it 'do not allow user moderate not own quests' do
          delete "/quests/#{@quest.id}"
          expect(response).to have_http_status(:forbidden)
        end
      end
    end
  end
end
