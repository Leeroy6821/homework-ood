require 'rails_helper'

RSpec.describe Quest::Destroy, tdd: true do
  let!(:current_user) { User.create(email: "test@test.com", password: "password") }
  let!(:params) { { name: "Pivorak Homework Quest", description: "homework quest", user_id: current_user.id } }

  it 'delete quest' do
    quest = described_class.call(params)

    expect(quest).to be_an_instance_of(Quest)
    expect(quest).to_not be_persisted
  end
end
