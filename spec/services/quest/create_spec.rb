require 'rails_helper'

RSpec.describe Quest::Create, tdd: true do
  let!(:current_user) { User.create(email: "test@test.com", password: "password") }
  let!(:params) { { name: "Pivorak Cool Quest", description: "really cool", user_id: current_user.id } }
  let!(:updated_params) { { name: "Not so good quest", description: "just bad", user_id: current_user.id } }

  it 'create quest' do
    quest = described_class.call(params)

    expect(Quest.find(quest.id)).to be_an_instance_of(Quest)
    expect(quest).to be_persisted
  end
end
