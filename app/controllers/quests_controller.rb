class QuestsController < ApplicationController
  def index
    quests = Quest.all
    render json: QuestSerializer.new(quests)
  end

  def create
    quest = Quest.new(quest_params)

    if quest.save
      render(json: 'Quest was successfully created'.to_json, status: :created)
    else
      render(json: quest.errors.full_messages, status: :unprocessable_entity)
    end
  end

  def show
    render json: QuestSerializer.new(quest)
  end

  def update
    return if quest.update(quest_params)

    render status: :unprocessable_entity
  end

  def destroy
    quest.delete
  end

  private

  def quest_params
    params.require(:quest).permit(:name, :description)
  end

  def quest
    @quest = Quest.find(params[:id])
  end
end
