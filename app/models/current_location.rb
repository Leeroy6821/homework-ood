class CurrentLocation < ApplicationRecord
  belongs_to :user
  validates :lat, :lon, presence: true
end
