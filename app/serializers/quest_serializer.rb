class QuestSerializer
  include FastJsonapi::ObjectSerializer

  attributes :id

  attribute :questName, &:name

  attribute :questDescription, &:description
end
