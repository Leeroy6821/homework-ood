class CreateCurrentLocations < ActiveRecord::Migration[5.2]
  def change
    create_table :current_locations do |t|
      t.string :lat
      t.string :lon

      t.timestamps
    end
  end
end
